const express = require('express');
const app = express();
const puppeteer = require('puppeteer-extra')
const pluginStealth = require('puppeteer-extra-plugin-stealth')
const {executablePath} = require('puppeteer');
const cheerio = require('cheerio');



app.get('/search', async (req, res) => {
  // Open a new browser and a new page
  const browser = await puppeteer.launch({
    headless: true,
    args: ['--no-sandbox','--disable-setuid-sandbox']
  });
  const page = await browser.newPage();

  // Navigate to the URL
  await page.goto('https://www.cargoloop.com/CLSearch.aspx');

  // Find the search field element and enter the text
  const searchField = await page.$('#SearchWord');
  //await searchField.type('WBSFV9C59FD594508');
  await searchField.type(req.query.q);

  // Submit the form
  const form = await page.$('#SearchBtn');
  await form.click();

  // Wait for the results to load
  await page.waitForSelector('#ResultDiv');

  // Print the HTML of the results
  const html = await page.$eval('#ResultDiv', el => el.querySelector('a').getAttribute('href'));
  const textInBrackets =  html.match(/\(([^)]+)\)/)[1];
  const textArray = textInBrackets.split(",");
  const viewID = textArray[0];

  // Close the browser
  await browser.close();

  // Send the HTML as a response
  res.set('Access-Control-Allow-Origin', '*');
  res.send(viewID);
});



app.get('/get-copart', async (req, res) => {
  puppeteer.use(pluginStealth());

  // Open a new browser and a new page
  const browser = await puppeteer.launch({
    headless: true,
    executablePath: executablePath(),
    args: ['--no-sandbox','--disable-setuid-sandbox']
  });

  const page = await browser.newPage();

  await page.setViewport({ width: 1280, height: 720 });

  // Add Headers
  await page.setExtraHTTPHeaders({
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'upgrade-insecure-requests': '1',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,en;q=0.8'
  });

  await page.goto(req.query.q, { waitUntil: 'networkidle0' });
  await page.waitForSelector('h1.title');
  const pageContent = await page.content();
  const $ = cheerio.load(pageContent);

  let copartJson = {};

  let trimText = function (userString) {
    return userString.trim().replace(/\n/g, '')
  }

  let parsePrice = function (priceString) {
    priceString = trimText(priceString);
    let numericPart = priceString.match(/[0-9,.]+/);

    if (priceString && numericPart) {
      numericPart = numericPart[0];
      return parseFloat(numericPart.replace(/,/g, ''));
    }
    else {
      return 0;
    }
  }

  copartJson.title =  $('h1.title').text();
  copartJson.lot = trimText($('#LotNumber').text());
  copartJson.imgURL = $('#show-img').attr('hd-url');
  copartJson.vinCode = trimText($('label[data-uname="lotdetailVin"]').next('div').find('.lot-details-desc').text());
  copartJson.titleCode = trimText($('span[data-uname="lotdetailTitledescriptionvalue"] > span > span').text());
  copartJson.odometer = trimText($('span[data-uname="lotdetailOdometervalue"] > span > span').text());
  copartJson.currentBid = parsePrice($('.bid-price').text());
  copartJson.buyNow = parsePrice($('.buyitnow-text .bid-price').text());
  copartJson.keys = trimText($('span[data-uname="lotdetailKeyvalue"]').text());
  copartJson.engine = trimText($('span[data-uname="lotdetailEnginetype"]').text());
  copartJson.fuel = trimText($('span[data-uname="lotdetailFuelvalue"]').text());
  copartJson.primaryDamage = trimText($('span[data-uname="lotdetailPrimarydamagevalue"]').text());
  copartJson.secondaryDamage = trimText($('span[data-uname="lotdetailSecondarydamagevalue"]').text());
  copartJson.location = trimText($('a[data-uname="lotdetailSaleinformationlocationvalue"]').text());



  // Close the browser
  await browser.close();

  // Send the HTML as a response
  res.set('Access-Control-Allow-Origin', '*');
  res.send(copartJson);
});

app.get('/get-iaai', async (req, res) => {
  puppeteer.use(pluginStealth());

  // Open a new browser and a new page
  const browser = await puppeteer.launch({
    headless: true,
    executablePath: executablePath(),
    args: ['--no-sandbox','--disable-setuid-sandbox']
  });

  const page = await browser.newPage();

  await page.setViewport({ width: 1280, height: 720 });

  // Add Headers
  await page.setExtraHTTPHeaders({
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36',
    'upgrade-insecure-requests': '1',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,en;q=0.8'
  });

  await page.goto(req.query.q, { waitUntil: 'networkidle0' });
  await page.waitForSelector('h1.heading-2');
  const pageContent = await page.content();
  const $ = cheerio.load(pageContent);

  let iaaiJson = {};

  let getValueByLabel = function (parentSelector, labelText) {
    const labelElements = $(parentSelector + '.data-list__label');

    for (let i = 0; i < labelElements.length; i++) {
      if ($(labelElements[i]).text().trim() === labelText) {
        const valueElement = $(labelElements[i]).nextAll('.data-list__value:first');
        return trimText(valueElement.text());
      }
    }

    return null; // Label not found
  }

  function getHrefByAnchorText(anchorText) {
    const anchorElements = $('a');

    for (let i = 0; i < anchorElements.length; i++) {
      if ($(anchorElements[i]).text().trim() === anchorText) {
        const hrefAttribute =  anchorElements[i].attribs.href;
        const regex = /\/locations\/(\d+)/;
        const match = hrefAttribute.match(regex);

        if (match) {
          return match[0]; // The entire match
        } else {
          return null;
        }
      }
    }

    return null; // Anchor not found
  }

  let trimText = function (userString) {
    return userString.trim().replace(/\n/g, '')
  }

  let parsePrice = function (priceString) {
    if (!priceString) {
      return 0;
    }

    priceString = trimText(priceString);
    let numericPart = priceString.match(/[0-9,.]+/);

    if (priceString && numericPart) {
      numericPart = numericPart[0];
      return parseFloat(numericPart.replace(/,/g, ''));
    }
    else {
      return 0;
    }
  }

  iaaiJson.title =  $('h1.heading-2').text();
  iaaiJson.lot = getValueByLabel('', 'Stock #:');
  iaaiJson.imgURL = $('#fullViewImg').attr('src');
  iaaiJson.vinCode =  getValueByLabel('', 'VIN (Status):');
  iaaiJson.titleCode = getValueByLabel('', 'Title/Sale Doc:');
  iaaiJson.odometer = getValueByLabel('', 'Odometer:');
  iaaiJson.currentBid = parsePrice(getValueByLabel('.action-area__secondary-info ', 'Current Bid:'));
  iaaiJson.buyNow = parsePrice(getValueByLabel('.action-area__secondary-info ', 'Buy Now Price:'));
  iaaiJson.keys = getValueByLabel('', 'Key:');
  iaaiJson.engine = getValueByLabel('', 'Engine:');
  iaaiJson.fuel = getValueByLabel('', 'Fuel Type:');
  iaaiJson.loss =  getValueByLabel('', 'Loss:');
  iaaiJson.primaryDamage = getValueByLabel('', 'Primary Damage:');
  iaaiJson.secondaryDamage = getValueByLabel('', 'Secondary Damage:');
  iaaiJson.location = getValueByLabel('', 'Selling Branch:');

  const locationURL = getHrefByAnchorText(iaaiJson.location);

  if (locationURL) {
    await page.goto('https://www.iaai.com' + locationURL, { waitUntil: 'networkidle0' });
    await page.waitForSelector('h1.heading-3');
    const pageContent = await page.content();
    const $ = cheerio.load(pageContent);

    iaaiJson.locationStateCode = $('span[itemprop="addressRegion"]').text();
    iaaiJson.locationStateName = $('span[itemprop="addressLocality"]').text();
  }

  // Close the browser
  await browser.close();

  // Send the HTML as a response
  res.set('Access-Control-Allow-Origin', '*');
  res.send(iaaiJson);
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
